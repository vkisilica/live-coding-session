<?php

declare(strict_types=1);

namespace Vladimir\Untitled;

abstract class MathFunctionAbstract implements MathFunctionInterface
{
    abstract public function handle(int $i): string;

    protected function dividesBy(int $i, int $divider): bool
    {
        return $i % $divider === 0;
    }
}