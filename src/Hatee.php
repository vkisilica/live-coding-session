<?php

declare(strict_types=1);

namespace Vladimir\Untitled;

class Hatee extends MathFunctionAbstract implements MathFunctionInterface
{
    public function handle($i): string
    {
        return $this->dividesBy($i, 2) ? 'hatee' : '';
    }
}