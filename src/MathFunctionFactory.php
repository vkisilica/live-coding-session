<?php

declare(strict_types=1);

namespace Vladimir\Untitled;

class MathFunctionFactory implements MathFunctionFactoryInterface
{
    public function create(string $functionName): MathFunctionInterface
    {
        switch ($functionName) {
            case 'pa':
                $function = new Pa();
                break;
            case 'pow':
                $function = new Pow();
                break;
            case 'hatee':
                $function = new Hatee();
                break;
            case 'ho':
                $function = new Ho();
                break;
            default:
                throw new \Exception('Function is not defined');
        }

        return $function;
    }
}