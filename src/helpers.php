<?php

declare(strict_types=1);

function rangeMod(int $from, int $to, array $dividers): Iterator {
    for ($i = $from; $i <= $to; ++$i) {
        $tmp = '';
        foreach ($dividers as $divider) {
            $factory = new \Vladimir\Untitled\MathFunctionFactory();
            $function = $factory->create($divider);

            $tmp .= $function->handle($i);
        }
        if ($tmp === '') {
            $tmp = (string)$i;
        }

        yield $tmp;
    }
}

function pa($i): string
{
    return Pa::dividesBy($i, 3) ? __FUNCTION__ : '';
}

function customPow($i): string
{
    return Pa::dividesBy($i, 5) ? 'pow' : '';
}

function ho($i): string
{
    return Pa::dividesBy($i, 7) ? __FUNCTION__ : '';
}

function hatee($i): string
{
    return Pa::dividesBy($i, 2) ? __FUNCTION__ : '';
}

//function isEven(int $i): bool
//{
//    return $i % 2 === 0;
//}
//
//function dividesBySeven(int $i): bool
//{
//    return $i % 7 === 0;
//}
//
//function dividesByFive(int $i): bool
//{
//    return $i % 5 === 0;
//}
//
//function dividesByThree(int $i): bool
//{
//    return $i % 3 === 0;
//}

