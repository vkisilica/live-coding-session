<?php

declare(strict_types=1);

namespace Vladimir\Untitled;

interface MathFunctionInterface
{
    public function handle(int $i): string;
}