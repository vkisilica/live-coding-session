<?php

declare(strict_types=1);

namespace Vladimir\Untitled;

interface MathFunctionFactoryInterface
{
    public function create(string $functionName): MathFunctionInterface;
}