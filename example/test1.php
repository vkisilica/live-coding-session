<?php

declare(strict_types=1);

require_once ".." . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";
require_once ".." . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . "helpers.php";

$results = rangeMod(1, 20, ['pa', 'pow']);
$firstKey = $results->key();
$separator = ' ';
foreach ($results as $key => $item) {
    if ($key === $firstKey) {
        echo $item;
        continue;
    }
    echo "$separator$item";
}